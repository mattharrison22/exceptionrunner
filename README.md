Tasks to answer in your own README.md that you submit on Canvas:

1. See logger.log, why is it different from the log to console?
   1. Console Log only shows Output in console and doesn't store anywhere, but logger will be able to be more specific with the results of a test and exceptions.
2. Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
   1. This line comes from the log, which reports on the warning messages, but in this case, the log result is "Fine", which is a level of log.
3. What does Assertions.assertThrows do?
   1. Assertions.assertThrows asserts that the code will execute, or it will throw an exception to the expected Assertions type.
4. See TimerException and there are 3 questions
    1. What is serialVersionUID and why do we need it? (please read on Internet)
       1. SerialVersionUID remembers version of the serialized class and is needed to make sure the same class is loaded during deserialization.
    2. Why do we need to override constructors?
       1. We need to override constructors in order to construct an exception with a message as opposed to the original constructor which returns a null.
    3. Why we did not override other Exception methods?	
       1. The other exception methods will not need to be overridden because there are multiple parameters which do not show up with the first two.
5. The Timer.java has a static block static {}, what does it do? (determine when called by debugger)
   1. The static block executes the code before the object of Timer is initialized.
6. What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
   1. an .md file is an extension of an HTML file which contains markdown syntax in the root level of a bitbucket repository.
7. Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
   1. The failOver test isn't working because even though it will throw the TimerException, the "finally" portion will still play which because the timeNow hadn't been initiated, a null exception will be thrown, which is not expected by the failOver test.
8. What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
   1. The actual issue is that the timeNow should not be null when checking, so the TimerException isn't called, and NullPointer is instead called, because the exception is called too soon so the handler has a problem in the finally section.
9. Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 
10. Make a printScreen of your eclipse Maven test run, with console
11. What category of Exceptions is TimerException and what is NullPointerException
    1. TimerException is in the INFO category, and the NullPointerException is in the ALL category.
12. Push the updated/fixed source code to your own repository.